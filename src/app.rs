use leptos::*;
use leptos_router::*;

#[component]
pub fn App(cx: Scope) -> impl IntoView {
    view! { cx,
        <Router>
            <header>
                <nav>
                    <A href="/">"Root"</A><br/>
                    <A href="/a">"/a"</A><br/>
                    <A href="/a/b">"/a/b"</A><br/>
                </nav>
            </header>
            <main>
                <Routes>
                    // This first set of routes works as expected, but obviously duplicates the /a
                    // prefix:
                    // <Route path="" view={|cx| "Root" }/>
                    // <Route path="/a" view={|cx| view! { cx, <CompA/> } }/>
                    // <Route path="/a/b" view={|cx| view! { cx, <CompB/> } }/>

                    // This second set of routes is intended to work like above, but remove the
                    // duplication by nesting the routes prefixed with /a. (Inspired by the example
                    // at https://docs.rs/leptos_router/latest/leptos_router/index.html.)
                    // However, whilst the URL changes on clicking the links, the <main> element
                    // always contains "Root".

                    <Route path="" view={|cx| view! { cx, <Root/> } }>
                        <Route path="" view={|cx| view! { cx, <SubRoot/> } }>
                        </Route>
                        <Route path="a" view={|cx| view! { cx, <CompA/> } }>
                            <Route path="" view={|cx| "Just A" }/>
                            <Route path="b" view={|cx| "Bee" }/>
                        </Route>
                    </Route>
                </Routes>
            </main>
        </Router>
    }
}

#[component]
fn Root(cx: Scope) -> impl IntoView {
    view! { cx,
        <div>
            <h1>"Rooty root"</h1>
            <Outlet/>
        </div>
    }
}

#[component]
fn SubRoot(cx: Scope) -> impl IntoView {
    view! { cx,
        <h1>"SubRoot"</h1>
        <Outlet/>
    }
}

#[component]
fn CompA(cx: Scope) -> impl IntoView {
    view! { cx,
        <div>
            <h2>"A"</h2>
            <Outlet/>
        </div>
    }
}

#[component]
fn CompB(cx: Scope) -> impl IntoView {
    view! { cx,
        <div>
            <h3>"B"</h3>
        </div>
    }
}
