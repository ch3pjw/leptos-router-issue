use {leptos::*, wasm_bindgen::prelude::*};

pub mod app;

use app::{App, AppProps};

#[wasm_bindgen]
pub fn hydrate() {
    leptos::mount_to_body(|cx| {
        view! { cx, <App/> }
    })
}
