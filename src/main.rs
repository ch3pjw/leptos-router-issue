#[cfg(feature = "ssr")]
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    use {
        actix_files::Files,
        actix_web::{App, HttpServer},
        leptos::{get_configuration, *},
        leptos_actix::{generate_route_list, LeptosRoutes},
    };

    let conf = get_configuration(None).await.unwrap();
    let addr = conf.leptos_options.site_addr;
    let app_view = |cx| {
        use leptos_router_issue::app::{App, AppProps};
        view! { cx, <App/> }
    };
    let routes = generate_route_list(app_view);

    HttpServer::new(move || {
        App::new()
            .leptos_routes(conf.leptos_options.to_owned(), routes.to_owned(), app_view)
            .service(Files::new("/", &conf.leptos_options.site_root))
    })
    .bind(&addr)?
    .run()
    .await
}

#[cfg(not(feature = "ssr"))]
fn main() {
    println!("Nothing to see here");
}
